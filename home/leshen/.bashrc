#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

export PATH="$PATH:/home/leshen/.komodoide/12.0/XRE/state" # ActiveState State Tool

export PATH="$PATH:/home/leshen/.komodoide/12.0/XRE/state" # ActiveState State Tool
